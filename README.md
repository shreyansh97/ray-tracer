# Raytracer using CUDA and MPI
The final working code is in final.cu

# Dependencies
CUDA and OpenMPI

### Compile
run the bash script `compile.sh`

### Execute
`mpirun -np number_of_processes ./a.out`
When running on a cluster include the `--hosts` flag with the different hosts on the cluster
